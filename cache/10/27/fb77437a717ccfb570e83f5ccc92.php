<?php

/* flashmessage.html.twig */
class __TwigTemplate_1027fb77437a717ccfb570e83f5ccc92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((array_key_exists("flash", $context) && $this->getAttribute($this->getContext($context, "flash", true), "type", array(), "any", true, true)) && $this->getAttribute($this->getContext($context, "flash", true), "message", array(), "any", true, true))) {
            // line 2
            echo "  <div class=\"alert alert-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "flash"), "type"), "html", null, true);
            echo "\">
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
    ";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "flash"), "message"), "html", null, true);
            echo "
  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "flashmessage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  21 => 2,  19 => 1,);
    }
}
