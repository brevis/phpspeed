<?php

/* flashmessage.html.twig */
class __TwigTemplate_69bef16b272b574b0987d48ba04671d540806a54dc9eb12dcfe9442c9c5066f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );

        $this->macros = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((array_key_exists("flash", $context) && $this->getAttribute($this->getContext($context, "flash", true), "type", array(), "any", true, true)) && $this->getAttribute($this->getContext($context, "flash", true), "message", array(), "any", true, true))) {
            // line 2
            echo "  <div class=\"alert alert-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "flash"), "type"), "html", null, true);
            echo "\">
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
    ";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "flash"), "message"), "html", null, true);
            echo "
  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "flashmessage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  24 => 2,  22 => 1,  152 => 48,  149 => 47,  143 => 46,  135 => 43,  129 => 40,  126 => 39,  120 => 38,  112 => 35,  106 => 32,  103 => 31,  97 => 30,  89 => 27,  83 => 24,  80 => 23,  74 => 22,  66 => 19,  60 => 16,  52 => 10,  50 => 9,  47 => 8,  44 => 7,  39 => 5,  33 => 3,);
    }
}
