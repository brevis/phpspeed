<?php

/* suggestions.html.twig */
class __TwigTemplate_0aea9250de89b2de45ce31fcb59284bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((array_key_exists("suggestions", $context) && (twig_length_filter($this->env, $this->getContext($context, "suggestions")) > 0))) {
            // line 2
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "suggestions"));
            foreach ($context['_seq'] as $context["_key"] => $context["test"]) {
                // line 3
                echo "        <div class=\"hover\" data-url=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tests", array("slug" => $this->getAttribute($this->getContext($context, "test"), "slug"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "test"), "title"), "html", null, true);
                echo "</div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['test'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 5
            echo "
    <script>
    suggestions_count = ";
            // line 7
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getContext($context, "suggestions")), "html", null, true);
            echo ";
    suggestions_hover = 0;
    </script>
";
        }
    }

    public function getTemplateName()
    {
        return "suggestions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 7,  37 => 5,  26 => 3,  21 => 2,  19 => 1,);
    }
}
