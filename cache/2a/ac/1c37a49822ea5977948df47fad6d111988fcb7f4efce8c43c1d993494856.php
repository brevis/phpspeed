<?php

/* add.html.twig */
class __TwigTemplate_2aac1c37a49822ea5977948df47fad6d111988fcb7f4efce8c43c1d993494856 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'site_title' => array($this, 'block_site_title'),
            'header' => array($this, 'block_header'),
            'javascripts' => array($this, 'block_javascripts'),
            'main' => array($this, 'block_main'),
        );

        $this->macros = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_site_title($context, array $blocks = array())
    {
        echo "Контакты";
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
    }

    // line 7
    public function block_javascripts($context, array $blocks = array())
    {
        // line 8
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script>
\$(document).ready(function() {
    var html = '<div class=\"form-group';
    ";
        // line 12
        if ($this->getAttribute($this->getContext($context, "errors", true), "super", array(), "any", true, true)) {
            echo "html +=' has-error';";
        }
        // line 13
        echo "    html += '\"><label for=\"formContent\">Вопрос со звездочкой* ';
    ";
        // line 14
        if ($this->getAttribute($this->getContext($context, "errors", true), "super", array(), "any", true, true)) {
            echo " html += ' <span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "super"), "html", null, true);
            echo "</span>';";
        }
        // line 15
        echo "    html += '</label><select class=\"form-control\" style=\"width:100px;\" name=\"form[super]\">';
    html += '<option></option>';
    html += '<option value=\"0\"';
    ";
        // line 18
        if (($this->getAttribute($this->getContext($context, "form"), "super") == "0")) {
            echo " html += ' selected=\"selected\"';";
        }
        // line 19
        echo "    html += '>0001011000111010</option>';
    html += '<option value=\"1\"';
    ";
        // line 21
        if (($this->getAttribute($this->getContext($context, "form"), "super") == "1")) {
            echo " html += ' selected=\"selected\"';";
        }
        // line 22
        echo "    html += '>Я не робот</option>';
    html += '</div>';
    \$('#addafter').after( html );
});
</script>
";
    }

    // line 29
    public function block_main($context, array $blocks = array())
    {
        // line 30
        echo "<div class=\"container\">    

    <div class=\"page-header\">
        <h1>Контакты</h1>
        <p class=\"lead\">На этой странице Вы можете предложить свой тест или просто написать нам что нибудь.</p>
    </div>

    ";
        // line 37
        $this->env->loadTemplate("flashmessage.html.twig")->display($context);
        // line 38
        echo "
    <form action=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add"), "html", null, true);
        echo "\" method=\"post\" class=\"container form-horizontal\" id=\"frmAdd\">
        <input type=\"hidden\" name=\"confirm\" value=\"ok\">
        
        <div class=\"form-group";
        // line 42
        if ($this->getAttribute($this->getContext($context, "errors", true), "author_name", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
            <label for=\"formAuthor_name\">
                Ваше имя*
                ";
        // line 45
        if ($this->getAttribute($this->getContext($context, "errors", true), "author_name", array(), "any", true, true)) {
            echo "<span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "author_name"), "html", null, true);
            echo "</span>";
        }
        // line 46
        echo "            </label>
            <input type=\"text\" name=\"form[author_name]\" class=\"form-control\" id=\"formAuthor_name\" value=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "author_name"), "html", null, true);
        echo "\" required autofocus>
        </div>

        <div class=\"form-group";
        // line 50
        if ($this->getAttribute($this->getContext($context, "errors", true), "author_email", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
            <label for=\"formAuthor_email\">
                Ваш email*
                ";
        // line 53
        if ($this->getAttribute($this->getContext($context, "errors", true), "author_email", array(), "any", true, true)) {
            echo "<span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "author_email"), "html", null, true);
            echo "</span>";
        }
        // line 54
        echo "            </label>
            <input type=\"email\" name=\"form[author_email]\" class=\"form-control\" id=\"formAuthor_email\" value=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "author_email"), "html", null, true);
        echo "\" required>
        </div>

        <div class=\"form-group";
        // line 58
        if ($this->getAttribute($this->getContext($context, "errors", true), "slug", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
            <label for=\"formAuthor_url\">
                Адрес вашего сайта/блога (если есть)
                ";
        // line 61
        if ($this->getAttribute($this->getContext($context, "errors", true), "Author_url", array(), "any", true, true)) {
            echo "<span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "Author_url"), "html", null, true);
            echo "</span>";
        }
        // line 62
        echo "            </label>
            <input type=\"text\" name=\"form[author_url]\" class=\"form-control\" id=\"formAuthor_url\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "author_url"), "html", null, true);
        echo "\">
        </div>
      
        <div class=\"form-group";
        // line 66
        if ($this->getAttribute($this->getContext($context, "errors", true), "content", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\" id=\"addafter\">
            <label for=\"formContent\">
                Ваше сообщение*
                ";
        // line 69
        if ($this->getAttribute($this->getContext($context, "errors", true), "content", array(), "any", true, true)) {
            echo "<span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "content"), "html", null, true);
            echo "</span>";
        }
        // line 70
        echo "            </label>
            <textarea class=\"form-control\" name=\"form[content]\" rows=\"20\" id=\"formContent\" required>";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "content"), "html", null, true);
        echo "</textarea>
        </div>


        <p>
            <button type=\"submit\" class=\"btn btn-default\">Отправить</button>
        </p>
        <br>
    </form>

</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 71,  200 => 70,  194 => 69,  186 => 66,  180 => 63,  177 => 62,  171 => 61,  163 => 58,  157 => 55,  154 => 54,  148 => 53,  140 => 50,  134 => 47,  131 => 46,  125 => 45,  117 => 42,  111 => 39,  108 => 38,  106 => 37,  97 => 30,  94 => 29,  85 => 22,  81 => 21,  77 => 19,  73 => 18,  68 => 15,  62 => 14,  59 => 13,  55 => 12,  48 => 8,  45 => 7,  40 => 5,  34 => 3,);
    }
}
