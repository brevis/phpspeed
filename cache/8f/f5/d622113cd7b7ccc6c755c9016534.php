<?php

/* search.html.twig */
class __TwigTemplate_8ff5d622113cd7b7ccc6c755c9016534 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'site_title' => array($this, 'block_site_title'),
            'header' => array($this, 'block_header'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_site_title($context, array $blocks = array())
    {
        echo "Результаты поиска по запросу \"";
        echo twig_escape_filter($this->env, $this->getContext($context, "str"), "html", null, true);
        echo "\"";
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\">

    <div class=\"page-header\">
        <h1>
            ";
        // line 12
        if (($this->getContext($context, "str") == "")) {
            echo "Поиск";
        } else {
            echo "<h1>Результаты поиска по запросу \"";
            echo twig_escape_filter($this->env, $this->getContext($context, "str"), "html", null, true);
            echo "\"";
        }
        // line 13
        echo "        </h1>
    </div>

    <form action=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("search"), "html", null, true);
        echo "\" class=\"form-inline searchform searchpage\">
        <div class=\"form-group\">
            <input type=\"text\" name=\"str\" class=\"form-control\" placeholder=\"Поиск...\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getContext($context, "str"), "html", null, true);
        echo "\">
            <button class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-search\"></span></button>
        </div>            
    </form>

    ";
        // line 23
        if ((array_key_exists("results", $context) && (twig_length_filter($this->env, $this->getContext($context, "results")) > 0))) {
            // line 24
            echo "        <ul>
        ";
            // line 25
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "results"));
            foreach ($context['_seq'] as $context["_key"] => $context["test"]) {
                // line 26
                echo "        <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tests", array("slug" => $this->getAttribute($this->getContext($context, "test"), "slug"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "test"), "title"), "html", null, true);
                echo "</li>        
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['test'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "        </ul>            
    ";
        } else {
            // line 30
            echo "        ";
            if (($this->getContext($context, "str") != "")) {
                echo "<p style=\"margin: 20px 0;\">ничего не найдено</p>";
            }
            // line 31
            echo "    ";
        }
        // line 32
        echo "
</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 32,  107 => 31,  102 => 30,  98 => 28,  87 => 26,  83 => 25,  80 => 24,  78 => 23,  70 => 18,  65 => 16,  60 => 13,  52 => 12,  46 => 8,  43 => 7,  38 => 5,  30 => 3,);
    }
}
