<?php

/* error.html.twig */
class __TwigTemplate_90c72b5305e8c3852b530f07c3cad49f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'site_title' => array($this, 'block_site_title'),
            'header' => array($this, 'block_header'),
            'main' => array($this, 'block_main'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_site_title($context, array $blocks = array())
    {
        echo "Ошибочка вышла";
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\" style=\"width: 600px;margin: 100px auto 0 auto;\">

    <h1>";
        // line 10
        echo twig_escape_filter($this->env, $this->getContext($context, "error"), "html", null, true);
        echo "</h1>    

</div> <!-- /container -->
";
    }

    // line 15
    public function block_footer($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 15,  49 => 10,  45 => 8,  42 => 7,  37 => 5,  194 => 129,  188 => 125,  180 => 120,  171 => 113,  160 => 111,  156 => 110,  151 => 107,  149 => 106,  135 => 94,  132 => 93,  127 => 91,  65 => 32,  34 => 4,  31 => 3,);
    }
}
