<?php

/* sidebar.html.twig */
class __TwigTemplate_de9c4472249ba574ace27ab68b65db3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"sidebar\">
    <img src=\"http://placehold.it/300x300\" alt=\"\">

    <div class=\"block\">
        <h4>Другие видео про Спорт</h4>
        <div class=\"thumbs\">
            <div class=\"thumb cf\">
                <a href=\"#\">
                    <img src=\"http://placehold.it/60x60\" alt=\"\">
                    <span class=\"desc\">Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе?</span>
                    <span class=\"date\">Добавлено 22 сентября 2013</span>
                </a>
            </div>
            <div class=\"thumb cf\">
                <a href=\"#\">
                    <img src=\"http://placehold.it/60x60\" alt=\"\">
                    <span class=\"desc\">Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе?</span>
                    <span class=\"date\">Добавлено 22 сентября 2013</span>
                </a>
            </div>
            <div class=\"thumb cf\">
                <a href=\"#\">
                    <img src=\"http://placehold.it/60x60\" alt=\"\">
                    <span class=\"desc\">Как размножаются морские ежи в неволе?</span>
                    <span class=\"date\">Добавлено 22 сентября 2013</span>
                </a>
            </div>
            <div class=\"thumb cf\">
                <a href=\"#\">
                    <img src=\"http://placehold.it/60x60\" alt=\"\">
                    <span class=\"desc\">Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе?</span>
                    <span class=\"date\">Добавлено 22 сентября 2013</span>
                </a>
            </div>
            <div class=\"thumb cf\">
                <a href=\"#\">
                    <img src=\"http://placehold.it/60x60\" alt=\"\">
                    <span class=\"desc\">Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе?</span>
                    <span class=\"date\">Добавлено 22 сентября 2013</span>
                </a>
            </div>
        </div>
    </div>
</div>

<div class=\"cf\"></div>";
    }

    public function getTemplateName()
    {
        return "sidebar.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  110 => 24,  105 => 21,  100 => 18,  94 => 14,  89 => 13,  86 => 12,  80 => 10,  75 => 9,  72 => 8,  66 => 5,  61 => 26,  59 => 24,  56 => 23,  54 => 21,  51 => 20,  49 => 18,  45 => 16,  42 => 12,  40 => 8,  36 => 7,  31 => 5,  25 => 1,);
    }
}
