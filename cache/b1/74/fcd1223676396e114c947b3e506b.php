<?php

/* admin/tests-edit.html.twig */
class __TwigTemplate_b174fcd1223676396e114c947b3e506b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("admin/base.html.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/js/jquery.synctranslit.min.js\"></script>
<script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/codemirror/lib/codemirror.js\"></script>
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/codemirror/lib/codemirror.css\">
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/codemirror/mode/xml/xml.js\"></script>
<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/codemirror/mode/htmlmixed/htmlmixed.js\"></script>
<script>
\$(document).ready(function(){
    ";
        // line 12
        if (($this->getContext($context, "id") == 0)) {
            echo "\$(\"#formTitle\").syncTranslit({destination: \"formSlug\"});";
        }
        // line 13
        echo "    
    var mixedMode = {
        name: \"htmlmixed\",
        scriptTypes: [{matches: /\\/x-handlebars-template|\\/x-mustache/i, mode: null},
                      {matches: /(text|application)\\/(x-)?vb(a|script)/i, mode: \"vbscript\"}]
    };
    var myCodeMirror = CodeMirror.fromTextArea( \$('#formContent').get(0), {
        mode: mixedMode,
        tabMode: \"indent\",
        lineWrapping: true,
        lineNumbers: true,
        extraKeys: {\"Ctrl-Space\": \"autocomplete\"}
    });
    myCodeMirror.setSize('100%', 500);
});
</script>
";
    }

    // line 31
    public function block_main($context, array $blocks = array())
    {
        // line 32
        echo "<div class=\"container main\">
    ";
        // line 33
        $this->env->loadTemplate("flashmessage.html.twig")->display($context);
        // line 34
        echo "    <form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-tests-edit"), "html", null, true);
        echo "?id=";
        echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
        echo "\" method=\"post\">
        <input type=\"hidden\" name=\"confirm\" value=\"ok\">
        ";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "filter"), "hidden"), "html", null, true);
        echo "
        <div class=\"page-header\">
            <h1 style=\"float: left\">";
        // line 38
        if (($this->getContext($context, "id") == 0)) {
            echo "Добавить";
        } else {
            echo "Изменить";
        }
        echo " тест</h1>
            <div class=\"buttons\">
                <button type=\"submit\" class=\"btn btn-success\">Сохранить</button>
                <a href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-tests"), "html", null, true);
        echo "?filter=";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "filter"), "url"), "html", null, true);
        echo "\" class=\"btn btn-danger\">Закрыть</a>
            </div>
            <div style=\"clear:both;\"></div>    
        </div>

        <div class=\"form-group";
        // line 46
        if ($this->getAttribute($this->getContext($context, "errors", true), "title", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
            <label for=\"formTitle\">
                Заголовок*
                ";
        // line 49
        if ($this->getAttribute($this->getContext($context, "errors", true), "title", array(), "any", true, true)) {
            echo "<span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "title"), "html", null, true);
            echo "</span>";
        }
        // line 50
        echo "            </label>
            <input type=\"text\" name=\"form[title]\" class=\"form-control\" id=\"formTitle\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "title"), "html", null, true);
        echo "\" required autofocus>
        </div>

        <div class=\"form-group";
        // line 54
        if ($this->getAttribute($this->getContext($context, "errors", true), "slug", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
            <label for=\"formSlug\">
                Slug*
                ";
        // line 57
        if ($this->getAttribute($this->getContext($context, "errors", true), "slug", array(), "any", true, true)) {
            echo "<span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "slug"), "html", null, true);
            echo "</span>";
        }
        // line 58
        echo "            </label>
            <input type=\"text\" name=\"form[slug]\" class=\"form-control\" id=\"formSlug\" value=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "slug"), "html", null, true);
        echo "\" required>
        </div>
      
        <div class=\"form-group";
        // line 62
        if ($this->getAttribute($this->getContext($context, "errors", true), "category", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
            <label for=\"formSlug\">
                Категория                
                ";
        // line 65
        if ($this->getAttribute($this->getContext($context, "errors", true), "category", array(), "any", true, true)) {
            echo "<span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "category"), "html", null, true);
            echo "</span>";
        }
        // line 66
        echo "            </label>
            <select name=\"form[category]\" class=\"form-control\">
            ";
        // line 68
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "categories"));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 69
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "c"), "html", null, true);
            echo "\"";
            if (($this->getContext($context, "c") == $this->getAttribute($this->getContext($context, "form"), "category"))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getContext($context, "c"), "html", null, true);
            echo "</option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "            </select>
        </div>

        <div class=\"form-group";
        // line 74
        if ($this->getAttribute($this->getContext($context, "errors", true), "author_name", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
            <label for=\"formSlug\">
                Имя автора
                ";
        // line 77
        if ($this->getAttribute($this->getContext($context, "errors", true), "author_name", array(), "any", true, true)) {
            echo "<span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "author_name"), "html", null, true);
            echo "</span>";
        }
        // line 78
        echo "            </label>
            <input type=\"text\" name=\"form[author_name]\" class=\"form-control\" id=\"formAuthor_name\" value=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "author_name"), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group";
        // line 82
        if ($this->getAttribute($this->getContext($context, "errors", true), "author_url", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
            <label for=\"formAuthor_url\">
                Сайт автора
                ";
        // line 85
        if ($this->getAttribute($this->getContext($context, "errors", true), "author_url", array(), "any", true, true)) {
            echo "<span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "author_url"), "html", null, true);
            echo "</span>";
        }
        // line 86
        echo "            </label>
            <input type=\"text\" name=\"form[author_url]\" class=\"form-control\" id=\"formAuthor_url\" value=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "author_url"), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"formFeatured\">
                Отображать тест на главной странице
            </label>
            <select name=\"form[featured]\" class=\"form-control\" style=\"width: 100px;\">
            <option value=\"0\"";
        // line 95
        if (($this->getAttribute($this->getContext($context, "form"), "featured") == "0")) {
            echo " selected=\"selected\"";
        }
        echo ">Нет</option>
            <option value=\"1\"";
        // line 96
        if (($this->getAttribute($this->getContext($context, "form"), "featured") == "1")) {
            echo " selected=\"selected\"";
        }
        echo ">Да</option>
            </select>
        </div>

        <div class=\"form-group";
        // line 100
        if ($this->getAttribute($this->getContext($context, "errors", true), "content", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
            <label for=\"formContent\">
                Контент
                ";
        // line 103
        if ($this->getAttribute($this->getContext($context, "errors", true), "content", array(), "any", true, true)) {
            echo "<span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "errors"), "content"), "html", null, true);
            echo "</span>";
        }
        // line 104
        echo "            </label>
            <textarea class=\"form-control\" name=\"form[content]\" rows=\"20\" id=\"formContent\">";
        // line 105
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "form"), "content"), "html", null, true);
        echo "</textarea>
        </div>

    </form>
</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "admin/tests-edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  294 => 105,  291 => 104,  285 => 103,  277 => 100,  268 => 96,  262 => 95,  251 => 87,  248 => 86,  242 => 85,  234 => 82,  228 => 79,  225 => 78,  219 => 77,  211 => 74,  206 => 71,  191 => 69,  187 => 68,  183 => 66,  177 => 65,  169 => 62,  163 => 59,  160 => 58,  154 => 57,  146 => 54,  140 => 51,  137 => 50,  131 => 49,  123 => 46,  113 => 41,  103 => 38,  98 => 36,  90 => 34,  88 => 33,  85 => 32,  82 => 31,  62 => 13,  58 => 12,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  32 => 4,  29 => 3,);
    }
}
