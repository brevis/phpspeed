<?php

/* base.html.twig */
class __TwigTemplate_a64f48caa548ecf4ba443660869a4533 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'site_title' => array($this, 'block_site_title'),
            'css' => array($this, 'block_css'),
            'javascripts' => array($this, 'block_javascripts'),
            'header' => array($this, 'block_header'),
            'main' => array($this, 'block_main'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <title>";
        // line 5
        $this->displayBlock('site_title', $context, $blocks);
        echo "</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <link rel=\"shortcut icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "favicon.ico\">
    ";
        // line 8
        $this->displayBlock('css', $context, $blocks);
        // line 12
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 16
        echo "</head>
<body>
";
        // line 18
        $this->displayBlock('header', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('main', $context, $blocks);
        // line 23
        echo "    
";
        // line 24
        $this->displayBlock('footer', $context, $blocks);
        // line 29
        echo "</body>
</html>";
    }

    // line 5
    public function block_site_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "app"), "site_title"), "html", null, true);
    }

    // line 8
    public function block_css($context, array $blocks = array())
    {
        // line 9
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\">
    <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/styles.css\" rel=\"stylesheet\" media=\"screen\">
    ";
    }

    // line 12
    public function block_javascripts($context, array $blocks = array())
    {
        // line 13
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/js/jquery.min.js\"></script>
    <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/js/bootstrap.min.js\"></script>
    ";
    }

    // line 18
    public function block_header($context, array $blocks = array())
    {
    }

    // line 21
    public function block_main($context, array $blocks = array())
    {
    }

    // line 24
    public function block_footer($context, array $blocks = array())
    {
        // line 25
        echo "<div class=\"container\">
    <p><a href=\"/\">&larr; На главную</a></p>
</div>
";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 25,  110 => 24,  105 => 21,  100 => 18,  94 => 14,  89 => 13,  86 => 12,  80 => 10,  75 => 9,  72 => 8,  66 => 5,  61 => 29,  59 => 24,  56 => 23,  54 => 21,  51 => 20,  49 => 18,  45 => 16,  42 => 12,  40 => 8,  36 => 7,  31 => 5,  25 => 1,);
    }
}
