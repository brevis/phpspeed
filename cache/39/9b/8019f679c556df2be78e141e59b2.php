<?php

/* explore.html.twig */
class __TwigTemplate_399b8019f679c556df2be78e141e59b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'site_title' => array($this, 'block_site_title'),
            'header' => array($this, 'block_header'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_site_title($context, array $blocks = array())
    {
        echo "Все тесты";
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\">

    <div class=\"page-header\">
        <h1>Все тесты</h1>
    </div>    

    ";
        // line 14
        if ((array_key_exists("alltests", $context) && (twig_length_filter($this->env, $this->getContext($context, "alltests")) > 0))) {
            // line 15
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "alltests"));
            foreach ($context['_seq'] as $context["title"] => $context["tests"]) {
                // line 16
                echo "        <h3>";
                echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
                echo "</h3>
        <ul>
        ";
                // line 18
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getContext($context, "tests"));
                foreach ($context['_seq'] as $context["_key"] => $context["test"]) {
                    // line 19
                    echo "        <li><a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tests", array("slug" => $this->getAttribute($this->getContext($context, "test"), "slug"))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "test"), "title"), "html", null, true);
                    echo "</li>        
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['test'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 21
                echo "        </ul>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['title'], $context['tests'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "    ";
        } else {
            // line 24
            echo "        ";
            if (($this->getContext($context, "str") != "")) {
                echo "<p style=\"margin: 20px 0;\">тестов пока нет</p>";
            }
            // line 25
            echo "    ";
        }
        // line 26
        echo "
</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "explore.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 26,  95 => 25,  90 => 24,  87 => 23,  80 => 21,  69 => 19,  65 => 18,  59 => 16,  54 => 15,  52 => 14,  44 => 8,  41 => 7,  36 => 5,  30 => 3,);
    }
}
