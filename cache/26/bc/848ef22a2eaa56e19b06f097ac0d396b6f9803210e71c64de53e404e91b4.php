<?php

/* admin/base.html.twig */
class __TwigTemplate_26bc848ef22a2eaa56e19b06f097ac0d396b6f9803210e71c64de53e404e91b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'site_title' => array($this, 'block_site_title'),
            'css' => array($this, 'block_css'),
            'javascripts' => array($this, 'block_javascripts'),
            'mainmenu' => array($this, 'block_mainmenu'),
            'main' => array($this, 'block_main'),
        );

        $this->macros = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"utf-8\">
    <link rel=\"shortcut icon\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "favicon.ico\">
    <title>";
        // line 6
        $this->displayBlock('site_title', $context, $blocks);
        echo "</title>
    ";
        // line 7
        $this->displayBlock('css', $context, $blocks);
        // line 30
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 33
        echo "    
  </head>

  <body>

    ";
        // line 38
        $this->displayBlock('mainmenu', $context, $blocks);
        // line 64
        echo "
    

    ";
        // line 67
        $this->displayBlock('main', $context, $blocks);
        // line 69
        echo "
  </body>
</html>
";
    }

    // line 6
    public function block_site_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "app"), "site_title"), "html", null, true);
    }

    // line 7
    public function block_css($context, array $blocks = array())
    {
        // line 8
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\">
    <style>
    .main{
      margin-top: 60px;
    }
    .page-header{
      margin: 0;
      padding: 0;
      margin-bottom: 20px;
    }    
    .page-header .buttons{
      padding-top: 20px;
      float: right;
    }
    .alert{      
      margin-bottom: 0;
    }    
    .CodeMirror-wrap {
      border: 1px solid #ddd;
    }
    </style>
    ";
    }

    // line 30
    public function block_javascripts($context, array $blocks = array())
    {
        echo "    
    <script src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/js/jquery.min.js\"></script>
    <script src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/js/bootstrap.min.js\"></script>
    ";
    }

    // line 38
    public function block_mainmenu($context, array $blocks = array())
    {
        // line 39
        echo "    <!-- Fixed navbar -->
    <div class=\"navbar navbar-default navbar-fixed-top\">
      <div class=\"container\">
        <div class=\"navbar-header\">
          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
          <a class=\"navbar-brand\" href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-index"), "html", null, true);
        echo "\">PHP Speed</a>
        </div>
        <div class=\"navbar-collapse collapse\">
          <ul class=\"nav navbar-nav\">
            <li";
        // line 52
        if (($this->getContext($context, "active_page") == "admin-tests")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-tests"), "html", null, true);
        echo "\">Тесты</a></li>
            <li";
        // line 53
        if (($this->getContext($context, "active_page") == "admin-pages")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-pages"), "html", null, true);
        echo "\">Страницы</a></li>
            <li";
        // line 54
        if (($this->getContext($context, "active_page") == "admin-views")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-views"), "html", null, true);
        echo "\">Шаблоны</a></li>
          </ul>
          <ul class=\"nav navbar-nav navbar-right\">
            <li><a href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "\">Перейти на фронтэнд</a></li>
            <li><a href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-logout"), "html", null, true);
        echo "\">Выйти</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    ";
    }

    // line 67
    public function block_main($context, array $blocks = array())
    {
        // line 68
        echo "    ";
    }

    public function getTemplateName()
    {
        return "admin/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 68,  181 => 67,  171 => 58,  167 => 57,  157 => 54,  149 => 53,  141 => 52,  134 => 48,  123 => 39,  120 => 38,  114 => 32,  110 => 31,  105 => 30,  75 => 7,  69 => 6,  60 => 67,  46 => 33,  43 => 30,  41 => 7,  37 => 6,  33 => 5,  27 => 1,  104 => 36,  100 => 34,  96 => 32,  84 => 28,  78 => 8,  70 => 24,  66 => 22,  62 => 69,  55 => 64,  53 => 38,  45 => 10,  40 => 7,  38 => 6,  34 => 4,  31 => 3,);
    }
}
