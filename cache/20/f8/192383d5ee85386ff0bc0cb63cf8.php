<?php

/* index.html.twig */
class __TwigTemplate_20f8192383d5ee85386ff0bc0cb63cf8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'header' => array($this, 'block_header'),
            'main' => array($this, 'block_main'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script>
var suggestions_count = 0,
    suggestions_hover = 0;

\$(function() {
    var oldStr = '';    

    \$('.searchform input[type=\"text\"]').on('keyup', function(){
        showSuggestions();            
    });

    \$('.searchform input[type=\"text\"]').on('click', function(){
        \$(this).select();
    });

    function showSuggestions() {
        var str = \$.trim( \$('.searchform input[type=\"text\"]').val() );
        if ( str == '' ) {
            hideSuggestions();
            oldStr = str;
            return;
        }
        if ( str.length >= 3 && oldStr != str ) {
            \$.ajax({
                url: \"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("suggestions"), "html", null, true);
        echo "?str=\"+encodeURIComponent(str)
            }).done(function(data) {
                \$('#suggestions').html(data).show();
            });
        }
        oldStr = str;            
    }

    function hideSuggestions() {
        \$('#suggestions').hide();
        suggestions_count = 0;
    }

    \$(document).on('keydown', function(e){
        if( e.keyCode == 40 ) { // down
            e.preventDefault();
            \$('#suggestions div:nth-child('+(suggestions_hover+1)+')').removeClass('hover');
            suggestions_hover++;
            if ( suggestions_hover >= suggestions_count ) suggestions_hover = 0;
            \$('#suggestions div:nth-child('+(suggestions_hover+1)+')').addClass('hover');
        }

        if( e.keyCode == 38 ) { // up
            e.preventDefault();
            \$('#suggestions div:nth-child('+(suggestions_hover+1)+')').removeClass('hover');
            suggestions_hover--;
            if ( suggestions_hover < 0 ) suggestions_hover = suggestions_count-1;
            \$('#suggestions div:nth-child('+(suggestions_hover+1)+')').addClass('hover');   
        }

        if( e.keyCode == 13 ) { // enter            
            if ( suggestions_count == 0 ) return;
            e.preventDefault();
            document.location.href = \$('#suggestions div:nth-child('+(suggestions_hover+1)+')').data('url');
        }

        if( e.keyCode == 27 ) { // esc
            e.preventDefault();
            hideSuggestions();
        }
    });

    \$('.searchform .example span').click(function(){
        \$('.searchform input[type=\"text\"]').val( \$(this).text() );
        showSuggestions();
    })

    \$(document).on('click', '#suggestions div', function(e) {
        e.preventDefault();
        document.location.href = \$(this).data('url');
    });   
    
    \$(document).on('click', function() {
        hideSuggestions();
    });   

});     
</script>
";
    }

    // line 89
    public function block_header($context, array $blocks = array())
    {
    }

    // line 91
    public function block_main($context, array $blocks = array())
    {
        // line 92
        echo "<div class=\"container\">

    <form action=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("search"), "html", null, true);
        echo "\" class=\"form-inline searchform\">
        <h1>PHP Speed</h1>
        <div class=\"form-group\">
            <input type=\"text\" name=\"str\" class=\"form-control\" placeholder=\"Поиск...\" autofocus autocomplete=\"off\">
            <button class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-search\"></span></button>
            <div class=\"example\">Пример: <span>str_replace</span></div>
            <div id=\"suggestions\"></div>
        </div>            
    </form>

    ";
        // line 104
        if ((array_key_exists("featured", $context) && (twig_length_filter($this->env, $this->getContext($context, "featured")) > 0))) {
            // line 105
            echo "    <div class=\"featured\">
        <h4>Интересные тесты</h4>
        <ul>
        ";
            // line 108
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "featured"));
            foreach ($context['_seq'] as $context["_key"] => $context["test"]) {
                // line 109
                echo "        <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tests", array("slug" => $this->getAttribute($this->getContext($context, "test"), "slug"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "test"), "title"), "html", null, true);
                echo "</li>        
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['test'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 111
            echo "        </ul>

        <div class=\"row footer\">
            <div class=\"pull-left\">
                <a href=\"explore/\">Все тесты</a> &middot; 
                <a href=\"add/\">Предложить тест</a></div>
            <div class=\"pull-right\">
                <a href=\"";
            // line 118
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pages", array("slug" => "about")), "html", null, true);
            echo "\">О проекте</a>
            </div>
        </div>
    </div>
    ";
        }
        // line 123
        echo "
</div> <!-- /container -->
";
    }

    // line 127
    public function block_footer($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 127,  189 => 123,  181 => 118,  172 => 111,  161 => 109,  157 => 108,  152 => 105,  150 => 104,  137 => 94,  133 => 92,  130 => 91,  125 => 89,  62 => 29,  34 => 4,  31 => 3,);
    }
}
