<?php

/* post.html.twig */
class __TwigTemplate_1bad654b7625dd74be647b70a5901dfa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'site_title' => array($this, 'block_site_title'),
            'header' => array($this, 'block_header'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_site_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "post"), "title"), "html", null, true);
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\">

    <div class=\"page-header\">
        <h1>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "post"), "title"), "html", null, true);
        echo "</h1>
        ";
        // line 12
        if ($this->getAttribute($this->getContext($context, "post", true), "author_name", array(), "any", true, true)) {
            // line 13
            echo "            ";
            if (($this->getAttribute($this->getContext($context, "post"), "author_name") != "")) {
                // line 14
                echo "            <p class=\"lead\">Автор: ";
                if (($this->getAttribute($this->getContext($context, "post"), "author_url") != "")) {
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "post"), "author_url"), "html", null, true);
                    echo "\" target=\"_blank\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "post"), "author_name"), "html", null, true);
                    echo "</a>";
                } else {
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "post"), "author_name"), "html", null, true);
                }
                echo "</p>
            ";
            }
            // line 16
            echo "        ";
        }
        // line 17
        echo "    </div>

    <div>
        ";
        // line 20
        echo $this->getAttribute($this->getContext($context, "post"), "content");
        echo "
    </div>    

</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "post.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 20,  75 => 17,  72 => 16,  58 => 14,  55 => 13,  53 => 12,  49 => 11,  44 => 8,  41 => 7,  36 => 5,  30 => 3,);
    }
}
