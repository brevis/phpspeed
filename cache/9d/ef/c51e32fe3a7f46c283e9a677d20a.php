<?php

/* test.html.twig */
class __TwigTemplate_9defc51e32fe3a7f46c283e9a677d20a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'site_title' => array($this, 'block_site_title'),
            'header' => array($this, 'block_header'),
            'main' => array($this, 'block_main'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_site_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "test"), "title"), "html", null, true);
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\">

    <div class=\"page-header\">
        <h1>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "test"), "title"), "html", null, true);
        echo "</h1>
        ";
        // line 12
        if (($this->getAttribute($this->getContext($context, "test"), "author_name") != "")) {
            // line 13
            echo "        <p class=\"lead\">Автор: ";
            if (($this->getAttribute($this->getContext($context, "test"), "author_url") != "")) {
                echo "<a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "test"), "author_url"), "html", null, true);
                echo "\" target=\"_blank\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "test"), "author_name"), "html", null, true);
                echo "</a>";
            } else {
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "test"), "author_name"), "html", null, true);
            }
            echo "</p>
        ";
        }
        // line 14
        echo "        
    </div>

    ";
        // line 17
        echo $this->getAttribute($this->getContext($context, "test"), "content");
        echo "

</div> <!-- /container -->
";
    }

    // line 22
    public function block_footer($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 22,  75 => 17,  70 => 14,  56 => 13,  54 => 12,  50 => 11,  45 => 8,  42 => 7,  37 => 5,  31 => 3,);
    }
}
