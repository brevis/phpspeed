<?php

/* post.html.twig */
class __TwigTemplate_bfe0bc2b80c549a27f7d2956abe6c11bdb41d79ff625fe3015044a313b529c99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'site_title' => array($this, 'block_site_title'),
            'header' => array($this, 'block_header'),
            'main' => array($this, 'block_main'),
        );

        $this->macros = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_site_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "post"), "title"), "html", null, true);
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"container\">

    <div class=\"page-header\">
        <h1>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "post"), "title"), "html", null, true);
        echo "</h1>
        ";
        // line 12
        if ($this->getAttribute($this->getContext($context, "post", true), "author_name", array(), "any", true, true)) {
            // line 13
            echo "            ";
            if (($this->getAttribute($this->getContext($context, "post"), "author_name") != "")) {
                // line 14
                echo "            <p class=\"lead\">Автор: ";
                if (($this->getAttribute($this->getContext($context, "post"), "author_url") != "")) {
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "post"), "author_url"), "html", null, true);
                    echo "\" target=\"_blank\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "post"), "author_name"), "html", null, true);
                    echo "</a>";
                } else {
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "post"), "author_name"), "html", null, true);
                }
                echo "</p>
            ";
            }
            // line 16
            echo "        ";
        }
        // line 17
        echo "    </div>

    <div>
        ";
        // line 20
        echo $this->getAttribute($this->getContext($context, "post"), "content");
        echo "
    </div>    

</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "post.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 20,  78 => 17,  75 => 16,  61 => 14,  58 => 13,  56 => 12,  52 => 11,  47 => 8,  44 => 7,  39 => 5,  33 => 3,);
    }
}
