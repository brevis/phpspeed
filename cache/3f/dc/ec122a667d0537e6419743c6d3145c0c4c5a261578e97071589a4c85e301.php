<?php

/* feedbackemail.html.twig */
class __TwigTemplate_3fdcec122a667d0537e6419743c6d3145c0c4c5a261578e97071589a4c85e301 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );

        $this->macros = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<b>Имя:</b><br>
";
        // line 2
        echo twig_escape_filter($this->env, $this->getContext($context, "author_name"), "html", null, true);
        echo "<br><br>

<b>Email:</b><br>
";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "author_email"), "html", null, true);
        echo "<br><br>

";
        // line 7
        if (($this->getContext($context, "author_url") != "")) {
            // line 8
            echo "<b>Url:</b><br>
";
            // line 9
            echo twig_escape_filter($this->env, $this->getContext($context, "author_url"), "html", null, true);
            echo "<br><br>
";
        }
        // line 11
        echo "
<b>Сообщение:</b><br>
";
        // line 13
        echo nl2br(twig_escape_filter($this->env, $this->getContext($context, "content"), "html", null, true));
        echo "<br><br>";
    }

    public function getTemplateName()
    {
        return "feedbackemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 13,  46 => 11,  41 => 9,  38 => 8,  36 => 7,  31 => 5,  25 => 2,  22 => 1,);
    }
}
