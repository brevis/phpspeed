<?php

/* category.html.twig */
class __TwigTemplate_7825c876d48bd4656db0a6729f6dd64b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">

    <div class=\"main\">

        <h2>Спорт</h2>

        <div class=\"sort cf\">
            <div class=\"sorttype\">
                <span class=\"prefix\">Сортировка:</span>
                <b>Дата</b> <span class=\"sep\">|</span> <a href=\"/detube/browse/?orderby=title\" title=\"Sort by Title\" class=\"title\"><i>Название</i></a> <span class=\"sep\">|</span> <a href=\"/detube/browse/?orderby=views\" title=\"Sort by Views\" class=\"views\"><i>Просмотры</i></a>
            </div>

            <div class=\"view\">
                <a href=\"#\" class=\"list active\"><i></i></a>
                <a href=\"#\" class=\"grid\"><i></i></a>
            </div>
        </div>

        <div class=\"posts\">
            <div class=\"post cf\">
                <a href=\"#\"><img src=\"http://placehold.it/300x169\" alt=\"\"></a>
                <h3><a href=\"#\">Как размножаются морские ежи в неволе?</a></h3>
                <span class=\"author\">Добавил <a href=\"\">Вася</a> 20 сентября 2013</span>

                <p class=\"desc\">Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе?</p>

                <p class=\"stats\"><span class=\"views\"><i class=\"count\">48.60K</i></p>
            </div>
            <div class=\"post cf\">
                <a href=\"#\"><img src=\"http://placehold.it/300x169\" alt=\"\"></a>
                <h3><a href=\"#\">Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе?</a></h3>
                <span class=\"author\">Добавил <a href=\"\">Вася</a> 20 сентября 2013</span> 

                <p class=\"desc\">Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе?</p>

                <p class=\"stats\"><span class=\"views\"><i class=\"count\">48.60K</i></p>
            </div>
            <div class=\"post cf\">
                <a href=\"#\"><img src=\"http://placehold.it/300x169\" alt=\"\"></a>
                <h3><a href=\"#\">Как размножаются морские ежи в неволе?</a></h3>
                <span class=\"author\">Добавил <a href=\"\">Вася</a> 20 сентября 2013</span>                

                <p class=\"desc\">Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе? Как размножаются морские ежи в неволе?</p>

                <p class=\"stats\"><span class=\"views\"><i class=\"count\">48.60K</i></p>
            </div>
        </div>
        
    </div>

    ";
        // line 54
        $this->env->loadTemplate("sidebar.html.twig")->display($context);
        // line 55
        echo "
</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "category.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 55,  83 => 54,  31 => 4,  28 => 3,);
    }
}
