<?php

/* admin/index.html.twig */
class __TwigTemplate_780ea88bf8ebc763a30291c137d0e8c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("admin/base.html.twig");

        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container main\">

  <div class=\"page-header\">
    <h1 style=\"float: left\">Тесты</h1>
    <div class=\"buttons\">
        <a href=\"\" class=\"btn btn-success\">Добавить</a>
    </div>
    <div style=\"clear:both;\"></div>    
  </div>

    <table>
    <tr>
        <th>Slug</th>
        <th>Заголовок</th>
        <th>Действия</th>
    </tr>
    <tr>
        <td>Slug</td>
        <td>Заголовок</td>
        <td>Действия</td>
    </tr>
  </table>

</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "admin/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,);
    }
}
