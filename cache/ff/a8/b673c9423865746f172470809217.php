<?php

/* admin/tests.html.twig */
class __TwigTemplate_ffa8b673c9423865746f172470809217 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("admin/base.html.twig");

        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container main\">
    ";
        // line 6
        $this->env->loadTemplate("flashmessage.html.twig")->display($context);
        // line 7
        echo "    <div class=\"page-header\">
        <h1 style=\"float: left\">Тесты</h1>
        <div class=\"buttons\">
            <a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-tests-edit"), "html", null, true);
        echo "\" class=\"btn btn-success\">Добавить</a>
        </div>
        <div style=\"clear:both;\"></div>    
    </div>

    ";
        // line 15
        if ((array_key_exists("tests", $context) && (twig_length_filter($this->env, $this->getContext($context, "tests")) > 0))) {
            // line 16
            echo "    <table class=\"table table-striped table-bordered\"> 
    <tr>
        <th>Заголовок</th>
        <th style=\"width: 120px;\">Действия</th>
    </tr>
    ";
            // line 21
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "tests"));
            foreach ($context['_seq'] as $context["_key"] => $context["test"]) {
                // line 22
                echo "    <tr>        
        <td>
            <a href=\"";
                // line 24
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-tests-edit", array("id" => $this->getAttribute($this->getContext($context, "test"), "id"))), "html", null, true);
                echo "?filter=";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "filter"), "url"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "test"), "title"), "html", null, true);
                echo "</a> &middot;
            <a href=\"";
                // line 25
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("tests", array("slug" => $this->getAttribute($this->getContext($context, "test"), "slug"))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-share\"></span></a>
        </td>
        <td>
            <a class=\"btn btn-danger btn-small btn-xs\" href=\"";
                // line 28
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-tests-delete", array("id" => $this->getAttribute($this->getContext($context, "test"), "id"))), "html", null, true);
                echo "?filter=";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "filter"), "url"), "html", null, true);
                echo "\" onclick=\"return confirm('Вы уверены, что хотите удалить тест?');\"><span class=\"glyphicon glyphicon-trash\"></span> Удалить</a>
        </td>
    </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['test'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "  </table>
  ";
        } else {
            // line 34
            echo "    тестов нет
  ";
        }
        // line 36
        echo "
</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "admin/tests.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 36,  97 => 34,  93 => 32,  81 => 28,  75 => 25,  67 => 24,  63 => 22,  59 => 21,  52 => 16,  50 => 15,  42 => 10,  37 => 7,  35 => 6,  31 => 4,  28 => 3,);
    }
}
