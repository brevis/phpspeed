<?php

/* admin/login.html.twig */
class __TwigTemplate_086a0fbf8aa5002be8a4a5028c95dae9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("admin/base.html.twig");

        $this->blocks = array(
            'site_title' => array($this, 'block_site_title'),
            'mainmenu' => array($this, 'block_mainmenu'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_site_title($context, array $blocks = array())
    {
        echo "cp42";
    }

    // line 4
    public function block_mainmenu($context, array $blocks = array())
    {
    }

    // line 6
    public function block_main($context, array $blocks = array())
    {
        // line 7
        echo "<div class=\"container main\" style=\"width: 600px;\">

  <div class=\"page-header\">
    <h1>cp42</h1>    
  </div>

  ";
        // line 13
        if ((array_key_exists("form_error", $context) && ($this->getContext($context, "form_error") != ""))) {
            echo "<div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->getContext($context, "form_error"), "html", null, true);
            echo "</div>";
        }
        // line 14
        echo "
  <form action=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->getContext($context, "login_action"), "html", null, true);
        echo "\" role=\"form\" method=\"POST\">
    <div class=\"form-group\">
      <input type=\"text\" name=\"username\" class=\"form-control\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getContext($context, "username"), "html", null, true);
        echo "\" placeholder=\"Username\" autofocus rquired>
    </div>
    <div class=\"form-group\">
      <input type=\"password\" name=\"password\" class=\"form-control\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getContext($context, "password"), "html", null, true);
        echo "\" placeholder=\"Password\" rquired>
    </div>    
    <button type=\"submit\" class=\"btn btn-default\">Login</button>
  </form>

</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "admin/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 20,  66 => 17,  61 => 15,  58 => 14,  52 => 13,  44 => 7,  41 => 6,  36 => 4,  30 => 3,);
    }
}
