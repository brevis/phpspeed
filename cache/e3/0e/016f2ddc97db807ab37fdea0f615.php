<?php

/* admin/views.html.twig */
class __TwigTemplate_e30e016f2ddc97db807ab37fdea0f615 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("admin/base.html.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/codemirror/lib/codemirror.js\"></script>
<link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/codemirror/lib/codemirror.css\">
<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/codemirror/mode/xml/xml.js\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/codemirror/mode/css/css.js\"></script>
<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home"), "html", null, true);
        echo "views/codemirror/mode/htmlmixed/htmlmixed.js\"></script>
<script>
\$(document).ready(function(){  
    var mixedMode = {
        name: \"htmlmixed\",
        scriptTypes: [{matches: /\\/x-handlebars-template|\\/x-mustache/i, mode: null},
                      {matches: /(text|application)\\/(x-)?vb(a|script)/i, mode: \"vbscript\"}]
    };
    var myCodeMirror = CodeMirror.fromTextArea( \$('#formContent').get(0), {
        mode: mixedMode,
        tabMode: \"indent\",
        lineWrapping: true,
        lineNumbers: true,
        extraKeys: {\"Ctrl-Space\": \"autocomplete\"}
    });
    myCodeMirror.setSize('100%', 500);
});
</script>
";
    }

    // line 29
    public function block_main($context, array $blocks = array())
    {
        // line 30
        echo "<div class=\"container main\">
    ";
        // line 31
        $this->env->loadTemplate("flashmessage.html.twig")->display($context);
        // line 32
        echo "    <form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-views"), "html", null, true);
        echo "?tpl=";
        echo twig_escape_filter($this->env, $this->getContext($context, "tpl"), "html", null, true);
        echo "\" method=\"post\">
        <input type=\"hidden\" name=\"confirm\" value=\"ok\">
        <div class=\"page-header\">
            <h1 style=\"float: left\">Шаблоны</h1>
            <div class=\"buttons\">
                <button type=\"submit\" class=\"btn btn-success\">Сохранить</button>
            </div>
            <div style=\"clear:both;\"></div>    
        </div>

        <div class=\"row\">
            <div class=\"col-md-3\">
                <div class=\"list-group\">
                    ";
        // line 45
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "tpls"));
        foreach ($context['_seq'] as $context["k"] => $context["t"]) {
            // line 46
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin-views"), "html", null, true);
            echo "?tpl=";
            echo twig_escape_filter($this->env, $this->getContext($context, "k"), "html", null, true);
            echo "\" class=\"list-group-item";
            if (($this->getContext($context, "tpl") == $this->getContext($context, "k"))) {
                echo " active";
            }
            echo "\">";
            echo twig_escape_filter($this->env, $this->getContext($context, "k"), "html", null, true);
            echo "</a>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['k'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "                    
                </div>
            </div>

            <div class=\"col-md-9\">
                <div class=\"form-group\">
                    <textarea class=\"form-control\" name=\"content\" rows=\"20\" id=\"formContent\">";
        // line 53
        echo twig_escape_filter($this->env, $this->getContext($context, "content"), "html", null, true);
        echo "</textarea>
                </div>
            </div>
        </div>        

    </form>
</div> <!-- /container -->
";
    }

    public function getTemplateName()
    {
        return "admin/views.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 53,  123 => 47,  106 => 46,  102 => 45,  83 => 32,  81 => 31,  78 => 30,  75 => 29,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  32 => 4,  29 => 3,);
    }
}
