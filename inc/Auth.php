<?php
class Auth
{
    protected static $_instance = null;
    protected $_app = null;

    protected function __construct($app)
    {
        $this->_app = $app;
    }

    public static function getInstance($app) 
    {        
        if ( null === self::$_instance ) {
            self::$_instance = new self($app);
        }        
      
        return self::$_instance;
    }   

    public function validateCredentials($username, $password)
    {
        $authdata = $this->_app['config']['auth_data'];
        return isset($authdata[$username]) && $authdata[$username] === $password;
    } 

    public function checkAuth()
    {
        $need_login = true;
        $ses = $this->_app['session']->get('islogged');
        if ( !empty($ses) ) {
            foreach ($this->_app['config']['auth_data'] as $u=>$p) {
                if ( $ses == md5($u.':'.$p) ) {
                    $need_login = false;
                    break;
                }
            }
        }

        return $need_login;
    }

    public function createAuth($username, $password)
    {
        $this->_app['session']->set( 'islogged', md5($username.':'.$password) );
    }

    public function destroyAuth()
    {
        $this->_app['session']->set( 'islogged', '' );
    }
    
}