<?php
use Symfony\Component\HttpFoundation\Request;

class Utils
{
    protected static $_instance = null;
    protected $_app = null;
    protected $_request = null;

    protected function __construct($app)
    {
        $this->_app = $app;
        $this->_request = Request::createFromGlobals();
    }

    public static function getInstance($app) 
    {        
        if ( null === self::$_instance ) {
            self::$_instance = new self($app);
        }        
      
        return self::$_instance;
    }   

    public function setFlashMessage($message, $type='info', $direct=false)
    {        
        if ( !in_array($type, array('success', 'info', 'warning', 'danger')) ) $type = 'info';
        $obj = array('type' => $type, 'message' => $message);
        if ( $direct ) return $obj;
        $this->_app['session']->set('flash', $obj);
    }

}