<?php
use Symfony\Component\HttpFoundation\Request;

class Tests
{
    protected static $_instance = null;
    protected $_app = null;
    protected $_request = null;

    protected function __construct($app)
    {
        $this->_app = $app;
        $this->_request = Request::createFromGlobals();
    }

    public static function getInstance($app) 
    {        
        if ( null === self::$_instance ) {
            self::$_instance = new self($app);
        }        
      
        return self::$_instance;
    }   

    public function getTestById($id)
    {
        return $this->_app['db']->fetchAssoc("SELECT * FROM tests WHERE id = ? LIMIT 1", array(intval($id)));
    }

    public function getTestBySlug($slug)
    {
        return $this->_app['db']->fetchAssoc("SELECT * FROM tests WHERE slug = ? LIMIT 1", array($slug));
    }

    public function getTests($filter=false)
    {
        return $this->_app['db']->fetchAll("SELECT id, category, slug, title FROM tests");
    }

    public function getSuggestions($str)
    {
        $str = trim($str);
        if ( empty($str) || mb_strlen($str, 'UTF-8') < 3 ) return false;

        $str = mb_strtolower($str, 'UTF-8');

        return $this->_app['db']->fetchAll("SELECT id, slug, title FROM tests WHERE title_idx LIKE :title LIMIT 10", array('title'=>'%'.$str.'%'));
    }

    public function searchTests($str)
    {
        $str = trim($str);
        if ( empty($str) || mb_strlen($str, 'UTF-8') < 3 ) return false;

        $str = mb_strtolower($str, 'UTF-8');

        return $this->_app['db']->fetchAll("SELECT id, slug, title FROM tests WHERE title_idx LIKE :title", array('title'=>'%'.$str.'%'));
    }    

    public function saveTest($form, $id=0)
    {
        $form['title_idx'] = mb_strtolower($form['title'], 'UTF-8');

        $id = intval($id);
        if ( $id == 0) { // insert
            $this->_app['db']->insert('tests', $form);
            $id = intval( $this->_app['db']->lastInsertId() ); 
        } else { // update
            $this->_app['db']->update('tests', $form, array('id'=>$id));
        }

        return $id;
    }

    public function deleteTestById($id)
    {
        $this->_app['db']->delete( 'tests', array( 'id' => intval($id) ) );
    }

    public function getFeaturedTests($filter=false)
    {
        return $this->_app['db']->fetchAll("SELECT id, category, slug, title FROM tests WHERE featured='1'");
    }
    
    public function getForm($name='form', $method='')
    {
        if ( empty($name) ) $name = 'form';

        $method = strtolower($method);

        $fields = array('category', 'slug', 'title', 'content', 'author_name', 'author_url', 'featured');
        if ( $method == 'post' ) {
            $form = isset($_POST[$name]) ? $_POST[$name] : array();
        } elseif ( $method == 'get' ) {
            $form = isset($_GET[$name]) ? $_GET[$name] : array();
        }
        foreach ($fields as $k) {
            if ( !isset($form[$k]) ) $form[$k] = '';
        }

        if ( $form['featured'] != '1' ) $form['featured'] = '0';
        $form['title'] = trim($form['title']);        
        $form['slug'] = trim($form['slug'], "\n -");
        $form['author_name'] = trim($form['author_name']);
        $form['author_url'] = trim($form['author_url']);

        return $form;
    }

    public function validateForm($form, $id=0)
    {
        $errors = array();

        if ( !isset($form['title']) || empty($form['title']) ) $errors['title'] = 'Обязательное поле';
        if ( !isset($form['slug']) || empty($form['slug']) ) $errors['slug'] = 'Обязательное поле';
        if ( $id > 0 ) {
            $_ = $this->_app['db']->fetchAssoc("SELECT id FROM tests WHERE slug = ? AND id <> ? LIMIT 1", array($form['slug'], $id));
            if ( is_array($_) ) $errors['slug'] = 'Тест с таким адресом уже существует';
        }

        return $errors;
    }

    public function buildFilter()
    {        

        $filter = array(
            'filter' => '',
            'raw' => '',
            'hidden' => '',
            'url' => '',
            'p' => '',
        );
        

        $filter['p'] = intval( $this->_request->query->get('filter[p]') );

        return $filter;
    }

}