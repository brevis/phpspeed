<?php
use Symfony\Component\HttpFoundation\Request;

class Pages
{
    protected static $_instance = null;
    protected $_app = null;
    protected $_request = null;

    protected function __construct($app)
    {
        $this->_app = $app;
        $this->_request = Request::createFromGlobals();
    }

    public static function getInstance($app) 
    {        
        if ( null === self::$_instance ) {
            self::$_instance = new self($app);
        }        
      
        return self::$_instance;
    }   

    public function getPageById($id)
    {
        return $this->_app['db']->fetchAssoc("SELECT * FROM pages WHERE id = ? LIMIT 1", array(intval($id)));
    }

    public function getPageBySlug($slug)
    {
        return $this->_app['db']->fetchAssoc("SELECT * FROM pages WHERE slug = ? LIMIT 1", array($slug));
    }

    public function savePage($form, $id=0)
    {
        $id = intval($id);
        if ( $id == 0) { // insert
            $this->_app['db']->insert('pages', $form);
            $id = intval( $this->_app['db']->lastInsertId() ); 
        } else { // update
            $this->_app['db']->update('pages', $form, array('id'=>$id));
        }

        return $id;
    }

    public function deletePageById($id)
    {
        $this->_app['db']->delete( 'pages', array( 'id' => intval($id) ) );
    }

    public function getPages($filter)
    {
        return $this->_app['db']->fetchAll("SELECT id, slug, title FROM pages");
    }
    
    public function getForm($name='form', $method='')
    {
        if ( empty($name) ) $name = 'form';

        $method = strtolower($method);

        $fields = array('slug', 'title', 'content');
        if ( $method == 'post' ) {
            $form = isset($_POST[$name]) ? $_POST[$name] : array();
        } elseif ( $method == 'get' ) {
            $form = isset($_GET[$name]) ? $_GET[$name] : array();
        }
        foreach ($fields as $k) {
            if ( !isset($form[$k]) ) $form[$k] = '';
        }

        $form['title'] = trim($form['title']);
        $form['slug'] = trim($form['slug'], "\n -");

        return $form;
    }

    public function validateForm($form, $id=0)
    {
        $errors = array();

        if ( !isset($form['title']) || empty($form['title']) ) $errors['title'] = 'Обязательное поле';
        if ( !isset($form['slug']) || empty($form['slug']) ) $errors['slug'] = 'Обязательное поле';
        if ( $id > 0 ) {
            $_ = $this->_app['db']->fetchAssoc("SELECT id FROM pages WHERE slug = ? AND id <> ? LIMIT 1", array($form['slug'], $id));
            if ( is_array($_) ) $errors['slug'] = 'Страница с таким адресом уже существует';
        }

        return $errors;
    }

    public function buildFilter()
    {        

        $filter = array(
            'filter' => '',
            'raw' => '',
            'hidden' => '',
            'url' => '',
            'p' => '',
        );
        

        $filter['p'] = intval( $this->_request->query->get('filter[p]') );

        return $filter;
    }

}