<?php
if ( php_sapi_name() == 'cli' ) set_time_limit(3600);

// hack
if ( isset($_SERVER['REQUEST_URI']) ) $_SERVER['REQUEST_URI'] = rtrim($_SERVER['REQUEST_URI'], '/') . '/'; 

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

$app = new Silex\Application();

$app['config'] = include( __DIR__ . '/config.php' );

// config
$ips = array('127.0.0.1', '::1', '195.78.105.19');
if ( isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], $ips) ) {
    $app['debug'] = true;
    ini_set('display_errors', 1);
    error_reporting(-1);
}

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_sqlite',
        'path'     => __DIR__ . '/db.sqlite',
    ),
));
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/views',
    'twig.options' => array('cache' => __DIR__.'/cache')
));
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Libs
$app['utils'] = Utils::getInstance($app);
$app['auth'] = Auth::getInstance($app);

/* Front-end */
/*--------------------------------------------------------------------------*/

$app->match('/', function (Request $request) use ($app) { 

    $cTests = Tests::getInstance($app);

    return $app['twig']->render(
        'index.html.twig', 
        array(
            'featured' => $cTests->getFeaturedTests()
        )
    );
})
->bind('home');

/*--------------------------------------------------------------------------*/

$app->match('/suggestions/', function (Request $request) use ($app) {

    $cTests = Tests::getInstance($app);

    return $app['twig']->render(
        'suggestions.html.twig', 
        array(
            'suggestions' => $cTests->getSuggestions($request->query->get('str'))
        )
    );

})
->method('GET')
->bind('suggestions');

/*--------------------------------------------------------------------------*/

$app->match('/page/{slug}/', function (Request $request, $slug) use ($app) {

    $cPages = Pages::getInstance($app);
    $page = $cPages->getPageBySlug($slug);
    if ( !$page ) {
        $app->abort(404, "Такая страница не найдена");
    }
    
    return $app['twig']->render(
        'post.html.twig', 
        array(
            'post' => $page
        )
    );


})
->method('GET')
->bind('pages');

/*--------------------------------------------------------------------------*/

$app->match('/test/{slug}/', function (Request $request, $slug) use ($app) {
        
    $cTests = Tests::getInstance($app);
    $test = $cTests->getTestBySlug($slug);
    if ( !$test ) {
        $app->abort(404, "Такая страница не найдена");
    }
    
    return $app['twig']->render(
        'post.html.twig', 
        array(
            'post' => $test
        )
    );

})
->method('GET')
->bind('tests');

/*--------------------------------------------------------------------------*/

$app->match('/search/', function (Request $request) use ($app) {
     
    $cTests = Tests::getInstance($app);

    $str = $request->query->get('str');

    return $app['twig']->render(
        'search.html.twig', 
        array(
            'results' => $cTests->searchTests($str),
            'str' => $str
        )
    );

})
->method('GET')
->bind('search');

/*--------------------------------------------------------------------------*/

$app->match('/explore/', function (Request $request) use ($app) {

    $cTests = Tests::getInstance($app);
    $_ = $cTests->getTests();
    $alltests = array();
    if ( is_array($_) && count($_) > 0 ) {
        foreach ($_ as $k=>$v)
        {
            if ( !isset($alltests[$v['category']]) ) $alltests[$v['category']] = array();
            $alltests[$v['category']][] = $v;
        }
    }

    return $app['twig']->render(
        'explore.html.twig', 
        array(
            'alltests' => $alltests,            
        )
    );

})
->bind('explore');

/*--------------------------------------------------------------------------*/

$app->match('/add/', function (Request $request) use ($app) {

    $errors = array();
    $flash = null;
    
    $form = array();
    $fields = array('author_name', 'author_email', 'author_url', 'content', 'super');
    foreach ($fields as $k) {
        $form[$k] = '';
    }

    if ( $request->request->get('confirm') == 'ok' ) {
        $post = isset($_POST['form']) ? $_POST['form'] : array();
        foreach ($fields as $k) {
            if ( isset($post[$k]) ) $form[$k] = $post[$k];
        }

        if ( $form['super'] != '1' ) $errors['super'] = 'Подумайте ещё немного';
        if ( empty($form['author_name']) ) $errors['author_name'] = 'Обязательное поле';
        if ( empty($form['author_email']) ) $errors['author_email'] = 'Обязательное поле';
        if ( !isset($errors['author_email']) && !filter_var($form['author_email'], FILTER_VALIDATE_EMAIL))
            $errors['author_email'] = 'Похоже это неправильный email';
        if ( empty($form['content']) ) $errors['content'] = 'Обязательное поле';
        
        if ( count($errors) == 0 ) {     

            $to = array(
                'brevis.ua@gmail.com',
                'admin@g63.ru',
                'czp@ukr.net',
            );
            
            $subject = '[PHP Speed] Feedback';
            $message = $app['twig']->render('feedbackemail.html.twig', $form);
            $headers = 'From: feedback@phpspeed.ru' . "\r\n" .
                'Content-type: text/html; charset=utf-8' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            foreach($to as $email) {
                mail($email, $subject, $message, $headers);    
            }

            $app['utils']->setFlashMessage('Ваше сообщение отправлено', 'success');
            return new RedirectResponse( $app['request']->getBasePath() . '/add/' );
        } 

        $flash = $app['utils']->setFlashMessage('Произошли ошибки', 'danger', true);
    }
    
    if ( $flash != null ) $app[ 'twig' ]->addGlobal( 'flash', $flash );

    return $app['twig']->render(
        'add.html.twig', 
        array(
            'errors' => $errors,
            'form' => $form,
        )
    );

})
->bind('add');

/* Admin */
/*--------------------------------------------------------------------------*/
$admin = $app['controllers_factory'];

$admin->match('/', function (Request $request) use ($app) { 
    return new RedirectResponse( $app['request']->getBasePath().'/cp42/tests/' );
})
->bind('admin-index');

/*--------------------------------------------------------------------------*/

$admin->match('/login/', function (Request $request) use ($app) { 
        
    $form_error = '';

    $username = $request->request->get('username');
    $password = $request->request->get('password');

    if ( !empty($username) && !empty($password) ) {
        if ( $app['auth']->validateCredentials($username, $password) ) {
            $app['auth']->createAuth($username, $password);
            $back = preg_replace('~(cp42|login)~', '', $request->query->get('back'));
            $back = trim($back, '/');
            return new RedirectResponse( $app['request']->getBasePath().'/cp42/'.$back );
        } else {
            $form_error = 'неправильно';
        }
    }

    return $app['twig']->render(
        'admin/login.html.twig', 
        array(
            'active_page' => 'admin-login',
            'login_action' => $_SERVER['REQUEST_URI'],
            'username' => $username,
            'password' => $password,
            'form_error' => $form_error
        )
    );
})
->bind('admin-login');

/*--------------------------------------------------------------------------*/

$admin->get('/logout/', function (Request $request) use ($app) { 

    $app['auth']->destroyAuth();
    return new RedirectResponse( $app['request']->getBasePath().'/cp42' );

})
->bind('admin-logout');

/*--------------------------------------------------------------------------*/

$admin->get('/tests/', function (Request $request) use ($app) { 

    $cTests = Tests::getInstance($app);
    $filter = $cTests->buildFilter();
    $tests = $cTests->getTests($filter);

    return $app['twig']->render(
        'admin/tests.html.twig', 
        array(
            'active_page' => 'admin-tests',
            'tests' => $tests,
            'filter' => $filter
        )
    );
})
->bind('admin-tests');

/*--------------------------------------------------------------------------*/

$admin->match('/tests/edit/', function (Request $request) use ($app) { 
    
    $cTests = Tests::getInstance($app);

    $filter = $cTests->buildFilter();

    $id = intval( $request->query->get('id') );
    if ( $id < 1 ) $id = 0;

    $errors = array();
    $flash = null;
    $form = $cTests->getForm();

    if ( $id > 0 ) {
        $form = $cTests->getTestById($id);
        if ( !$form ) {
            $app['utils']->setFlashMessage('Тест с ID='.$id.' не найден', 'warning');
            return new RedirectResponse( $app['request']->getBasePath() . '/cp42/tests/?filter='.$filter['raw'] );
        }
    }

    if ( $request->request->get('confirm') == 'ok' ) {
        $form = $cTests->getForm('form', 'post');        
        $errors = array_merge($errors, $cTests->validateForm($form, $id));
        if ( count($errors) == 0 ) {
            $id = intval( $cTests->saveTest($form, $id) );
            $app['utils']->setFlashMessage('Тест сохранен', 'success');
            return new RedirectResponse( $app['request']->getBasePath() . '/cp42/tests/edit/?id='.$id.'&filter='.$filter['raw'] );
        } 

        $flash = $app['utils']->setFlashMessage('Произошли ошибки', 'danger', true);
    }
    
    if ( $flash != null ) $app[ 'twig' ]->addGlobal( 'flash', $flash );

    return $app['twig']->render(
        'admin/tests-edit.html.twig', 
        array(
            'active_page' => 'admin-tests',
            'id' => $id,
            'filter' => $filter,
            'errors' => $errors,
            'form' => $form,
            'categories' => $app['config']['categories']
        )
    );
})
->bind('admin-tests-edit');

/*--------------------------------------------------------------------------*/

$admin->match('/tests/delete/', function (Request $request) use ($app) { 
    
    $cTests = Tests::getInstance($app);

    $filter = $cTests->buildFilter();

    $id = intval( $request->query->get('id') );
    if ( $id < 1 ) $id = 0;

    $cTests->deleteTestById($id);
    $app['utils']->setFlashMessage('Тест удален', 'success');
    return new RedirectResponse( $app['request']->getBasePath() . '/cp42/tests/?filter='.$filter['url'] );
})
->bind('admin-tests-delete');

/*--------------------------------------------------------------------------*/

$admin->get('/pages/', function (Request $request) use ($app) { 

    $cPages = Pages::getInstance($app);
    $filter = $cPages->buildFilter();
    $pages = $cPages->getPages($filter);

    return $app['twig']->render(
        'admin/pages.html.twig', 
        array(
            'active_page' => 'admin-pages',
            'pages' => $pages,
            'filter' => $filter
        )
    );
})
->bind('admin-pages');

/*--------------------------------------------------------------------------*/

$admin->match('/pages/edit/', function (Request $request) use ($app) { 
    
    $cPages = Pages::getInstance($app);

    $filter = $cPages->buildFilter();

    $id = intval( $request->query->get('id') );
    if ( $id < 1 ) $id = 0;

    $errors = array();
    $flash = null;
    $form = $cPages->getForm();

    if ( $id > 0 ) {
        $form = $cPages->getPageById($id);
        if ( !$form ) {
            $app['utils']->setFlashMessage('Страница с ID='.$id.' не найдена', 'warning');
            return new RedirectResponse( $app['request']->getBasePath() . '/cp42/pages/?filter='.$filter['raw'] );
        }
    }

    if ( $request->request->get('confirm') == 'ok' ) {
        $form = $cPages->getForm('form', 'post');        
        $errors = array_merge($errors, $cPages->validateForm($form, $id));
        if ( count($errors) == 0 ) {
            $id = intval( $cPages->savePage($form, $id) );
            $app['utils']->setFlashMessage('Страница сохранена', 'success');
            return new RedirectResponse( $app['request']->getBasePath() . '/cp42/pages/edit/?id='.$id.'&filter='.$filter['raw'] );
        } 

        $flash = $app['utils']->setFlashMessage('Произошли ошибки', 'danger', true);
    }
    
    if ( $flash != null ) $app[ 'twig' ]->addGlobal( 'flash', $flash );

    return $app['twig']->render(
        'admin/pages-edit.html.twig', 
        array(
            'active_page' => 'admin-pages',
            'id' => $id,
            'filter' => $filter,
            'errors' => $errors,
            'form' => $form,
        )
    );
})
->bind('admin-pages-edit');

/*--------------------------------------------------------------------------*/

$admin->match('/pages/delete/', function (Request $request) use ($app) { 
    
    $cPages = Pages::getInstance($app);

    $filter = $cPages->buildFilter();

    $id = intval( $request->query->get('id') );
    if ( $id < 1 ) $id = 0;

    $cPages->deletePageById($id);
    $app['utils']->setFlashMessage('Страница удалена', 'success');
    return new RedirectResponse( $app['request']->getBasePath() . '/cp42/pages/?filter='.$filter['url'] );
})
->bind('admin-pages-delete');

/*--------------------------------------------------------------------------*/

$admin->match('/views/', function (Request $request) use ($app) { 

    $tpls = array();
    foreach(glob(__DIR__ . '/views/*.{twig,css}', GLOB_BRACE) as $t) {
        $tpls[basename($t)] = $t;
    }

    $tpl = $request->query->get('tpl');
    if ( empty($tpl) ) $tpl = key($tpls);    
    if ( !isset($tpls[$tpl]) ) {
        $app['utils']->setFlashMessage('Шаблон не существует', 'warning');
        return new RedirectResponse( $app['request']->getBasePath() . '/cp42/views/' );
    }

    $content = file_get_contents($tpls[$tpl]);

    if ( $request->request->get('confirm') == 'ok' ) {
        $content = $request->request->get('content');
        file_put_contents($tpls[$tpl], $content);
        $app['utils']->setFlashMessage('Шаблон обновлен', 'success');
        return new RedirectResponse( $app['request']->getBasePath() . '/cp42/views/?tpl='.$tpl );
    }

    return $app['twig']->render(
        'admin/views.html.twig', 
        array(
            'active_page' => 'admin-views',
            'tpls' => $tpls,
            'tpl' => $tpl,
            'content' => $content
        )
    );
})
->bind('admin-views');

/* App events */
/*--------------------------------------------------------------------------*/

$app->before(function (Request $request) use ($app) {

    // check auth    
    if ( preg_match('~^/cp42~', $_SERVER['REQUEST_URI'])) {
        if ( $app['auth']->checkAuth() ) {
            if ( !preg_match('~^/cp42/(login|logout)~', $_SERVER['REQUEST_URI']) ) 
                return new RedirectResponse( 
                    $app['request']->getBasePath() . '/cp42/login/?back=' . urlencode($_SERVER['REQUEST_URI']) 
                );
        } else {
            if ( preg_match('~^/cp42/login~', $_SERVER['REQUEST_URI']) ) 
                return new RedirectResponse( $app['request']->getBasePath() . '/cp42/' );
        }
    }
    
    // flash messages
    $flash = $app[ 'session' ]->get( 'flash' );
    $app[ 'session' ]->set( 'flash', null );
    if ( !empty( $flash ) ) $app[ 'twig' ]->addGlobal( 'flash', $flash );

    // $app['db']->query('PRAGMA journal_mode=WAL;');
    // $app['db']->query('PRAGMA synchronous = NORMAL;');

    // global tpl vars
    foreach ($app['config']['global_tpl_vars'] as $k=>$v) {
        if ( !isset($app[$k]) ) $app[$k] = $v;
    }

});

/* Errors */
/*--------------------------------------------------------------------------*/
$app->error(function (\Exception $e, $code) use ($app) {
    
    if ( $app['debug'] ) return;

    $data = array(
        'error' => 'Упс, что-то пошло не так...'
    );
    if ( $code == 404 ) {
        $data['error'] = 'Такая страница не найдена';
        return new Response( $app['twig']->render('error.html.twig', $data), 404) ;
    } else {
        return new Response( $app['twig']->render('error.html.twig', $data));
    }

});

/*--------------------------------------------------------------------------*/

$app->mount('/cp42/', $admin);

if ( php_sapi_name() == 'cli' && count($argv) >= 3 ) {
    list($_, $method, $path) = $argv;
    $request = Request::create($path, $method);
} else {
    $request = Request::createFromGlobals();
}

$app->run($request);
